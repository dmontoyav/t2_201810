package test.model.data_structures;

import model.data_structures.ListaEncadenadaDoble;

import com.sun.corba.se.impl.orbutil.graph.Node;

import junit.framework.TestCase;

public class ListaEncadenadaDobleTest extends TestCase{

	private ListaEncadenadaDoble lista;
	private Node nodoPrueba;


	public void setUp1()
	{
		lista= new ListaEncadenadaDoble();
		lista.addLast(1);
		lista.addLast(2);
		lista.addLast(3);
		lista.addLast(4);
		lista.addLast(5);
		lista.addLast(6);
		lista.addLast(7);
		lista.addLast(8);
		lista.addLast(9);
		lista.addLast(10);
		
		
		
	}

	public void testSize()
	{
		setUp1();
		assertEquals(10,lista.size());
	}

	public void testAdd()
	{
		setUp1();
		int num = 100;
		lista.addLast(num);
		assertEquals(100,lista.get(10));
	}

	public void testDelete()
	{
		setUp1();
		lista.delete(5);
		assertEquals(9,lista.size());
		assertEquals(7,lista.get(5));
	}

	public void testNext()
	{
		setUp1();
		assertEquals(2,lista.next());
	}

	public void testListing()
	{
		setUp1();
		lista.listing();
		assertEquals(1,lista.actual.getItem());

	}

	public void testGetCurrent()
	{
		setUp1();
		assertEquals(1,lista.getCurrent());
	}

	public void testGetElement()
	{
		setUp1();
		assertEquals(6,lista.get(5));
	}

	public void testGetPosition()
	{
		setUp1();
		assertEquals(5,lista.get(4));
	}
}

