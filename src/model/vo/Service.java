package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	public String trip_id;
	public String taxi_id;
	public int trip_seconds;
	public double trip_miles;
	public double trip_total;
	public int dropoff_community_area;
	
	public Service (String pTripId, String pTaxiId , int pTripSeconds, double pTripMiles , double pTripTotal, int pDropoffComunityArea)
	{
		
		trip_id =pTripId;
		taxi_id = pTaxiId;
		trip_seconds = pTripSeconds;
		trip_miles = pTripMiles;
		trip_total = pTripTotal;
		dropoff_community_area = pDropoffComunityArea;
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		return trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		return trip_total;
	}

	public int getDropOffCommunityArea()
	{
		return dropoff_community_area;
	}
	@Override
	public int compareTo(Service o) {
		int i = 0;
		if(getTripId().equals(o.getTripId()))
		{
			i=1;
		}
		return i;
	}
}
