package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	public String taxi_id;
	public String company; 
	
	public Taxi()
	{
		taxi_id = null;
		company = null;
	}
	public Taxi( String pTaxiId)
	{
		taxi_id = pTaxiId;
		company = null;
	}
	public Taxi(String pTaxiId , String pCompany)
	{
		taxi_id = pTaxiId;
		company = pCompany;
	}
	/**
	 * @return id - taxi_id
	 */
	
	public String getTaxiId() {
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) {
		int i ;
		if(getTaxiId().equals(o.getTaxiId()))
		{
			i=1;
		}
		else
		{
			i=0;
		}
		return i;
	}

}
