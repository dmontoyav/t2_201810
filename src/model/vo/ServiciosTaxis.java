package model.vo;

import model.data_structures.ListaEncadenadaDoble;

public class ServiciosTaxis implements Comparable<ServiciosTaxis>{
	public String trip_id;
	public String taxi_id;
	public int trip_seconds;
	public double trip_miles;
	public double trip_total;
	public int dropoff_community_area;
	public String company;
	
	public Service servicio;
	public Taxi taxi;
	
	public ServiciosTaxis (String pTripId, String pTaxiId , int pTripSeconds, double pTripMiles , double pTripTotal, int pDropoffComunityArea, String pCompany)
	{
		
		trip_id =pTripId;
		taxi_id = pTaxiId;
		trip_seconds = pTripSeconds;
		trip_miles = pTripMiles;
		trip_total = pTripTotal;
		dropoff_community_area = pDropoffComunityArea;
		company = pCompany;
	}
	
	public Service crearServicio()
	{
		servicio = new Service(trip_id, taxi_id, trip_seconds, trip_miles, trip_total, dropoff_community_area);
		return servicio;
	}
	public Taxi crearTaxi()
	{
		taxi = new Taxi(taxi_id,company);
		return taxi;
	}
	public String getTripId() {
		return trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		return trip_total;
	}

	public int getDropOffCommunityArea()
	{
		return dropoff_community_area;
	}
	
	/**
	 * @return company
	 */
	public String getCompany() {
		return company;
	}
	@Override
	public int compareTo(ServiciosTaxis o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
