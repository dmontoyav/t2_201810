package model.data_structures;

public class ListaEncadenadaSimple <T extends Comparable<T>>implements LinkedList<T> {
	
	private Node actual;
	private Node cabeza;
	private Node cola;
	private int size;
	@Override
	public void addLast(T elem) {
		if(elem != null)
		{
			Node nuevo = new Node(elem);
			if(cabeza==null)
			{
				cabeza =nuevo;
				actual = nuevo;
				cola = nuevo;
			}
			else
			{
				Node temp = cola;
				cola.cambiarSiguiente(temp);
				cola = temp;
			}
			size++;
		}
	}
	@Override
	public void delete(T elem)
	{
		if(elem != null)
		{
			int i = 0;
			actual = cabeza;
			while(!actual.getNext().getNext().getItem().equals(elem) && i <= size)
			{
				actual = actual.getNext();
				i++;
			}
			actual.cambiarSiguiente(actual.getNext().getNext());
			size--;
		}
	}
	@Override
	public T get(T elem) {
		actual = cabeza;
		int i = 0;
		while (!actual.getItem().equals(elem) && i <= size)
		{
			actual = actual.getNext();
			i++;
		}
		return (T)actual.getItem();
	}
	@Override
	public int size() {
		return size;
	}
	@Override
	public T get(int posicion) {
		actual = cabeza;
		int i = 0;
		while (i <= posicion)
		{
			actual = actual.getNext();
			i++;
		}
		return (T)actual.getItem();
	}
	@Override
	public void listing() {
		actual = cabeza;
		
	}
	@Override
	public T getCurrent() {
		return (T) actual.getItem();
	}
	@Override
	public T next() {
		return (T) actual.getNext().getItem();
	}
	
	
}
