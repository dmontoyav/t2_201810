package model.data_structures;

import java.util.Iterator;

public class ListaEncadenadaDoble <T extends Comparable<T>> implements Iterable, LinkedList<T>{

	private Node<T> cabeza;
	public Node<T> actual;
	private Node<T> cola;
	private int size;
	
	@Override
	//Agregar al final
	public void addLast(T elem) {
		if (elem != null)
		{
			Node<T> nuevo = new Node(elem);
			if(cabeza == null)
			{
				cabeza = nuevo;
				cola = nuevo;
				actual = nuevo;
				size++;
			}
			else
			{
				cola.cambiarSiguiente(nuevo);
				nuevo.cambiarAnterior(cola);;
				nuevo.cambiarSiguiente(null);
				cola=nuevo;
				size++;
			}
		}
	}

	@Override
	public void delete(T k) 
	{
		if (cabeza.getItem().equals(k))
		{
			Node temp = cabeza.getNext();
			cabeza = temp; 
		}
		else if (cola.getItem().equals(k)) 
		{
			Node temp = cola.getPrevious();
			cola = temp;
		}
		else
		{
			actual = cabeza.getNext();
			int i = 1;
			while(!actual.getItem().equals(k) && i < size)
			{
				actual = actual.getNext();
				i++;
			}
			Node anterior = actual.getPrevious();
			Node siguiente = actual.getNext();
			anterior.cambiarSiguiente(siguiente);
			siguiente.cambiarAnterior(anterior);
			actual = null;
		}
		
		
		size--;
	}

	@Override
	public T get(T elem) {
		actual = cabeza;
		int i = 0;
		while (!actual.getItem().equals(elem) && i < size)
		{
			actual = actual.getNext();
			i++;
		}
		return (T)actual.getItem();
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public T get(int posicion) {
		if (posicion < 0 || posicion > size)
		{
			System.out.println("Error");
			return null;
		}
		else if(posicion == 1)
		{
			return cabeza.getItem();
		}
		else if(posicion == size - 1)
		{
			return cola.getItem();
		}
		else
		{
			actual = cabeza;
			int i = 0;
			while (i < posicion)
			{
				actual = actual.getNext();
				i++;
			}
			return actual.getItem();
		}
	}

	@Override
	public void listing() 
	{
		actual = cabeza;	
	}

	@Override
	public T getCurrent() {
		return (T) actual.getItem();
	}

	@Override
	public T next() {
		T devolver = (T) actual.getNext().getItem();
		return devolver;
	}

	@Override
	public Iterator<T> iterator() {
		return (Iterator<T>) actual.getItem();
	}
	
	

}
