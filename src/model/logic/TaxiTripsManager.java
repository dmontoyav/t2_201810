package model.logic;

import java.io.FileReader;
import java.io.IOException;

import com.google.gson.Gson;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.vo.ServiciosTaxis;
import model.data_structures.LinkedList;
import model.data_structures.ListaEncadenadaDoble;


public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private LinkedList<Service> misServicios = new ListaEncadenadaDoble<Service>();
	private LinkedList<Taxi> misTaxis = new ListaEncadenadaDoble<Taxi>();
	
	public void loadServices (String serviceFile) 
	{	
		
		Gson gson = new Gson();
		try
		{
			ServiciosTaxis[] myTypes = gson.fromJson(new FileReader(serviceFile), ServiciosTaxis[].class);
			for( int i = 0 ; i < myTypes.length; i++)
			{
				misServicios.addLast(myTypes[i].crearServicio());
				boolean agregado = false;
				for(int j = 0 ; j < misTaxis.size() && agregado == false;j++)
				{
					if(myTypes[i].getTaxiId().equals(misTaxis.get(j).getTaxiId()))
					{
						agregado = true;
					}
				}
				if (!agregado)
				{
					misTaxis.addLast(myTypes[i].crearTaxi());
				}
			}
			System.out.println("1 " + misTaxis.size());
			System.out.println("2 " + misServicios.size());
		}
		catch(IOException e)
		{
			System.out.println("No importó los datos");
		}
		
	}

	/**public static LinkedList<Service> sort(LinkedList<Service> lista)
	{
		int tamano=0;
		tamano++;
		LinkedList<Service> aux = new ListaEncadenadaDoble<>();
		aux = lista;
		if(aux.size()<=1)
		{
			tamano--;
			return lista;
		}
		LinkedList<Service> izquierda = new ListaEncadenadaDoble<>();
		LinkedList<Service> derecha = new ListaEncadenadaDoble<>();
		LinkedList<Service> resultado = new ListaEncadenadaDoble<>();
		int mitad = lista.size()/2;
		int agregados = 0;
		for( int i = 0; i < lista.size();i++)
		{
			Service servicio = lista.get(i);
			if(agregados < mitad)
			{
				izquierda.addLast(servicio);
			}
			else
			{
				derecha.addLast(servicio);
			}
		}
		derecha = sort(derecha);
		izquierda = sort(izquierda);
		resultado = merge(izquierda,derecha);
		tamano--;
		return resultado;
	}


	public static LinkedList<Service> merge(LinkedList<Service> izquierda,LinkedList<Service> derecha)
	{
		LinkedList<Service> resultado = new ListaEncadenadaDoble<>();
		while(izquierda.size()>0 && derecha.size()>0)
		{
			if(izquierda.get(0).compareTo(derecha.get(0))<0)
			{
				Service actual = izquierda.get(0);
				resultado.addLast(actual);
			}
			else
			{
				Service actual = derecha.get(0);
				resultado.addLast(actual);
			}
		}
		if (izquierda.size()>0)
		{
			resultado.addLast(izquierda);
		}
		else
		{
			resultado.addLast(derecha);
		}
		return resultado;
	}
	*/
	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		LinkedList<Taxi> TaxisCompania = new ListaEncadenadaDoble<>();
		int i = 0 ;
		if(misTaxis.size()>0)
		{
			System.out.println("1");
			while(i < misTaxis.size())
			{
				Taxi actual = misTaxis.get(i);
				if(actual.getCompany()!=null && actual.getCompany().startsWith(company))
				{
					TaxisCompania.addLast(actual);
				}
				i++;
			}
		}
		
		System.out.println(TaxisCompania.size());
		return TaxisCompania;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		
		LinkedList<Service> ServiciosPorLugar = new ListaEncadenadaDoble<>();
		int i = 0;
		int j = 0;
		if(misServicios.size()>0)
		{	
			while(i<misServicios.size())
			{
				Service actual = misServicios.get(i);
				if(actual.getDropOffCommunityArea()==communityArea)
				{
					ServiciosPorLugar.addLast(actual);
					j++;
				}
				i++;
			}
			System.out.println(ServiciosPorLugar.size());
			System.out.println(j);
		}
		
		return ServiciosPorLugar;
	}
	
}
